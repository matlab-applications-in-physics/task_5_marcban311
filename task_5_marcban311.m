
clear;
%data files
G = 7.7e10;%shrea modul for stainless steel
r = 1/12 *2.54/100;%value of radius [m]
N=40;% number of coils 
m=0.1;%[kg]
t= 10;%const of suppres [s]
g=9.81;%gravity const [m/s^2]
Fg= m*g;

%exc_1

R_1 = 0.005;
k_1 = G * r^4 /(4* N* R_1^3);
omega_o = sqrt(k_1/m);

% show how amplitude change for different frequency for const. radius 
beta = 1/(2*t);
Am= [];
fsy= [];
for f = 0:0.1:100;
    omega = 2*pi*f;
    A_1 = Fg/(m*sqrt((omega_o^2-omega^2)^2+4*beta^2*omega^2));
    Am = [Am,A_1];
    fsy= [fsy,f];
   
end

subplot(3,1,1);
exc_1 = plot(fsy, Am)
xlabel('f [Hz]');
ylabel('A [m]');

%defining clear matrixses for the loop 
Am= [];
fs= [];
Rs =[];
beta = 1/(2*t);
As = [];
fst = [];
R = [0.005:0.002:0.05];
%loop which include different radius and frequency 
for R = 0.005:0.002:0.05
   % defining k for different value of radius 
   k_2 = G * r^4 /(4* N* R^3);
   omega_o = sqrt(k_2/m);
  
   for f = 0:0.1:200;
    omega = 2*pi*f;
    %defining amplitude for different radius and frequency 
    A = Fg/(m*sqrt((omega_o^2-omega^2)^2+4*beta^2*omega^2));
    Am=[Am,A];
    fs= [fs,f];
   end
   
    omega_2 = sqrt(omega_o^2 + beta^2);
    f_1 = omega_2/(2*pi);
    fst = [fst,f_1];
    
    %resonance amplitude
    A_1 = Fg/(m*sqrt((omega_o^2 - omega_2^2)^2+4*beta^2*omega_2^2));
    As= [As, A_1];
    Rs=[Rs, R];
   
end

%defining plots 
subplot(3,1,2);
exc_1 = plot(fs, Am);
xlabel('f [Hz]');
ylabel('A [m]');

subplot(3,1,3);
exc_2 = plot(Rs,As);
xlabel('Radius [m]');
ylabel('Amplitude [m]');

%plots to pdf 
saveas(gcf,'resonance.pdf');

% writing datafile 
Radius =[R]
Amplitude =[Am]
Resonance_amplitude=[As]
T = table(Radius, Amplitude, Resonance_amplitude)

writetable(T,'resonance_analysis_results.dat','WriteRowNames',true)
